﻿using UnityEngine;

public class PlayerMoveController : MonoBehaviour
{
    [SerializeField] private PlayerScaleController m_scaleController;
    [SerializeField] private CharacterController m_characterController;
    [SerializeField] private PlayerMoveBehaviour[] m_moveBehaviours;
    
    private void Update ()
    {
        if (!m_scaleController.IsScaling)
        {
            Vector3 moveVector = Vector3.zero;
            foreach (PlayerMoveBehaviour pmb in m_moveBehaviours)
            {
                moveVector += pmb.GetMoveVector();
            }
            m_characterController.Move(moveVector);
        }
    }
}
