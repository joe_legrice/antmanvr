﻿using UnityEngine;
using System.Collections;

public class InteractableItem : MonoBehaviour
{
    [SerializeField] private Animation m_animation;
    private bool m_didInteract;

    public bool CanInteract { get { return !m_didInteract; } }

    public void Interact()
    {
        if (!m_didInteract)
        {
            m_didInteract = true;
            m_animation.Play();
        }
    }
}
