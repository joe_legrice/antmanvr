﻿using UnityEngine;
using Rewired;

public class PlayerLateralMovementBehaviour : PlayerMoveBehaviour
{
    [SerializeField] private Transform m_cameraTransform;
    [SerializeField] private PlayerScaleController m_scaleController;
    [SerializeField] private float m_scaledUpSpeed;
    [SerializeField] private float m_scaledDownSpeed;
    
    private Player m_player;
    private Vector3 m_moveVector;

    private void Start()
    {
        m_player = ReInput.players.GetPlayer(0);
    }

    public override Vector3 GetMoveVector()
    {
        return m_moveVector;
    }

    private void Update()
    {
        Vector3 inputMoveVector = new Vector3();
        inputMoveVector.x = m_player.GetAxis("Strafe");
        inputMoveVector.z = m_player.GetAxis("Move");

        Vector3 forwardVector = Vector3.Cross(m_cameraTransform.right, Vector3.up);
        Quaternion forwardRotation = Quaternion.LookRotation(forwardVector, Vector3.up);
        float scaledSpeed = m_scaleController.IsAntSize ? m_scaledDownSpeed : m_scaledUpSpeed;
        m_moveVector = forwardRotation * inputMoveVector * scaledSpeed * Time.deltaTime;
    }
}
