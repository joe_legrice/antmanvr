﻿using UnityEngine;
using System.Collections;

public abstract class PlayerMoveBehaviour : MonoBehaviour
{
    public abstract Vector3 GetMoveVector();
}
