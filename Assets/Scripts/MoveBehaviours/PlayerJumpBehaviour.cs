﻿using UnityEngine;
using System.Collections;
using Rewired;

public class PlayerJumpBehaviour : PlayerMoveBehaviour
{
    [SerializeField] private PlayerScaleController m_scaleController;
    [SerializeField] private CharacterController m_characterController;
    [SerializeField] private AnimationCurve m_jumpDecayEasing;
    [SerializeField] private float m_jumpUpPowerScaledUp;
    [SerializeField] private float m_jumpUpPowerScaledDown;
    [SerializeField] private float m_jumpUpTime;
    [SerializeField] private float m_jumpDecayTime;

    private bool m_jumping;
    private Vector3 m_jumpVector;
    private Player m_player;

    public bool IsJumping { get { return m_jumping; } }

    public override Vector3 GetMoveVector()
    {
        return m_jumpVector * Time.deltaTime;
    }

    private void Start()
    {
        m_player = ReInput.players.GetPlayer(0);
    }

    private void Update()
    {
        if (m_characterController.isGrounded && m_player.GetButtonDown("Jump"))
        {
            StartCoroutine(Jump());
        }
    }

    private IEnumerator Jump()
    {
        if (!m_jumping)
        {
            m_jumping = true;
            m_jumpVector = GetJumpPowerVector();
            yield return new WaitForSeconds(m_jumpUpTime);

            float jumpDownTime = 0.0f;
            while (jumpDownTime < m_jumpDecayTime)
            {
                jumpDownTime += Time.deltaTime;
                float progress = Mathf.Clamp01(jumpDownTime / m_jumpDecayTime);
                m_jumpVector = Vector3.Lerp(GetJumpPowerVector(), Vector3.zero, m_jumpDecayEasing.Evaluate(progress));
                yield return null;
            }

            m_jumpVector = Vector3.zero;
            m_jumping = false;
        }
    }

    private Vector3 GetJumpPowerVector()
    {
        float power = m_scaleController.IsAntSize ? m_jumpUpPowerScaledDown : m_jumpUpPowerScaledUp;
        return power * Vector3.up;
    }
}
