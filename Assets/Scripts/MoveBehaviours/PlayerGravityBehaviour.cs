﻿using UnityEngine;

public class PlayerGravityBehaviour : PlayerMoveBehaviour
{
    [SerializeField] private PlayerScaleController m_scaleController;
    [SerializeField] private float m_scaledUpMass = 1.0f;
    [SerializeField] private float m_scaledDownMass = 0.25f;

    public override Vector3 GetMoveVector()
    {
        float mass = m_scaleController.IsAntSize ? m_scaledDownMass : m_scaledUpMass;
        return mass * Physics.gravity * Time.deltaTime;
    }
}
