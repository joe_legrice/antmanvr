﻿using UnityEngine;
using Rewired;
using System.Collections;

public class PlayerJetpackBehaviour : PlayerMoveBehaviour
{
    [SerializeField] private PlayerScaleController m_scaleController;
    [SerializeField] private float m_flyForce;
    [SerializeField] private float m_recoveryRate;
    [SerializeField] private float m_maxJetpackFlyTime;

    [SerializeField] private float m_decayTime;
    [SerializeField] private AnimationCurve m_decayCurve;

    private float m_timeInFlight;
    private Player m_player;
    private Vector3 m_jetpackMoveVector;

    public override Vector3 GetMoveVector()
    {
        return m_jetpackMoveVector;
    }

    private void Start()
    {
        m_player = ReInput.players.GetPlayer(0);
        StartCoroutine(Loop());
    }


    private IEnumerator Loop()
    {
        while (true)
        {
            if (m_scaleController.IsAntSize)
            {
                while (m_player.GetButton("Jump"))
                {
                    m_timeInFlight += Time.deltaTime;
                    if (m_timeInFlight < m_maxJetpackFlyTime)
                    {
                        m_jetpackMoveVector = Vector3.up * m_flyForce * Time.deltaTime;
                    }
                    else
                    {
                        m_jetpackMoveVector = Vector3.zero;
                    }
                    yield return null;
                }

                float initialForce = m_jetpackMoveVector.magnitude;
                float decayTimer = 0.0f;
                while (decayTimer < m_decayTime)
                {
                    decayTimer += Time.deltaTime;
                    float force = initialForce * m_decayCurve.Evaluate(decayTimer);
                    m_jetpackMoveVector = Vector3.up * initialForce;

                    m_timeInFlight = Mathf.Max(m_timeInFlight - m_recoveryRate * Time.deltaTime, 0.0f);

                    yield return null;
                }

                m_jetpackMoveVector = Vector3.zero;
                m_timeInFlight = Mathf.Max(m_timeInFlight - m_recoveryRate * Time.deltaTime, 0.0f);
            }
            else
            {
                m_jetpackMoveVector = Vector3.zero;
                m_timeInFlight = Mathf.Max(m_timeInFlight - m_recoveryRate * Time.deltaTime, 0.0f);
                yield return null;
            }
        }
    }
}
