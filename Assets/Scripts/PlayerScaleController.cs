﻿using UnityEngine;
using System.Collections;
using Rewired;

public class PlayerScaleController : MonoBehaviour
{
    [SerializeField] private GameObject m_particleSystemPrefab;
    [SerializeField] private Transform m_scaleTransform;
    [SerializeField] private Transform m_cameraTransform;
    [SerializeField] private AnimationCurve m_scaleEasing;

    [SerializeField] private float m_particleEffectDistance = 0.25f;
    [SerializeField] private float m_scaleTime;
    [SerializeField] private float m_normalScale = 1.0f;
    [SerializeField] private float m_antScale = 0.1f;

    public bool IsScaling { get { return m_isChangingSize; } }
    public bool IsAntSize { get { return m_antSized; } }

    private bool m_isChangingSize;
    private bool m_antSized;
    private Player m_player;
    private Vector3 m_initialScale;


    private void Start()
    {
        m_player = ReInput.players.GetPlayer(0);
        m_initialScale = m_scaleTransform.localScale;
    }

    private void Update()
    {
        if (m_player.GetButtonDown("ChangeScale"))
        {
            StartCoroutine(ChangeScale());
        }
    }

    private IEnumerator ChangeScale()
    {
        if (!m_isChangingSize)
        {
            GameObject particleSystemInstance = Instantiate(m_particleSystemPrefab);
            Transform particleSystemTransform = particleSystemInstance.GetComponent<Transform>();
            particleSystemTransform.position = m_cameraTransform.position + m_particleEffectDistance * Vector3.Cross(m_cameraTransform.right, Vector3.up);
            particleSystemTransform.rotation = Quaternion.LookRotation(Vector3.up, particleSystemTransform.position - m_cameraTransform.position);

            m_isChangingSize = true;
            m_antSized = !m_antSized;

            m_initialScale = m_scaleTransform.localScale;
            Vector3 targetScale = Vector3.one * (m_antSized ? m_antScale : m_normalScale);

            float timePassed = 0.0f;
            while (timePassed < m_scaleTime)
            {
                timePassed += Time.deltaTime;
                float progress = m_scaleEasing.Evaluate(Mathf.Clamp01(timePassed / m_scaleTime));
                m_scaleTransform.localScale = Vector3.Lerp(m_initialScale, targetScale, progress);
                particleSystemTransform.position = m_cameraTransform.position + m_particleEffectDistance * Vector3.Cross(m_cameraTransform.right, Vector3.up);
                particleSystemTransform.rotation = Quaternion.LookRotation(Vector3.up, particleSystemTransform.position - m_cameraTransform.position);

                yield return null;
            }
            m_scaleTransform.localScale = targetScale;
            m_isChangingSize = false;

            Destroy(particleSystemInstance);
        }
    }
}
