﻿using UnityEngine;
using Rewired;

public class PlayerLookController : MonoBehaviour
{
    [SerializeField] private PlayerScaleController m_scaleController;
    [SerializeField] private Texture2D m_interactionTexture;
    [SerializeField] private Camera m_camera;
    [SerializeField] private Transform m_cameraTransform;
    [SerializeField] private Transform m_yawRotationTransform;
    [SerializeField] private Transform m_pitchRotationTransform;
    [SerializeField] private float m_sensitivity;

    private Player m_player;
    private InteractableItem m_itemFound;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        m_player = ReInput.players.GetPlayer(0);
    }

    private void Update()
    {
#if UNITY_EDITOR
        MouseLook();
#endif
        if (!m_scaleController.IsAntSize)
        {
            Ray lookRay = new Ray(m_cameraTransform.position, m_cameraTransform.forward);
            RaycastHit hitInfo;
            if (Physics.Raycast(lookRay, out hitInfo, float.MaxValue))
            {
                InteractableItem ii = hitInfo.collider.GetComponent<InteractableItem>();
                if (((m_itemFound == null && ii != null) || m_itemFound != ii) && ii.CanInteract)
                {
                    m_itemFound = ii;
                    if (m_itemFound != null)
                    {
                        Cursor.SetCursor(m_interactionTexture, new Vector2(128, 128), CursorMode.Auto);
                    }
                }
            }

            if (m_itemFound != null)
            {
                if (m_player.GetButton("Interact"))
                {
                    m_itemFound.Interact();
                    m_itemFound = null;
                }
            }
            Cursor.visible = m_itemFound != null;
        }
        else
        {
            Cursor.visible = false;
        }

    }

    private void MouseLook()
    {
        float mouseY = m_player.GetAxis("LookVertical");
        Vector3 newPitchForward = m_pitchRotationTransform.forward + mouseY * m_sensitivity * Time.deltaTime * m_pitchRotationTransform.up;
        Quaternion newPitchRotation = Quaternion.LookRotation(newPitchForward);
        float angle = Quaternion.Angle(newPitchRotation, Quaternion.LookRotation(Vector3.up, -m_yawRotationTransform.forward));
        if (angle > 1.0f && angle < 179.0f)
        {
            m_pitchRotationTransform.rotation = newPitchRotation;
        }

        float mouseX = m_player.GetAxis("LookHorizontal");
        Vector3 newYawForward = m_yawRotationTransform.forward + mouseX * m_sensitivity * Time.deltaTime * m_yawRotationTransform.right;
        m_yawRotationTransform.rotation = Quaternion.LookRotation(newYawForward);
    }
}
